//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.DockTabSet.hpp>
#include <Vcl.Tabs.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------


struct FilesFormat {
	AnsiString fileName;
    AnsiString filePath;
	bool textFile;
	TTabSheet *TabSheet;
	TRichEdit *RichEdit;
};

class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TMainMenu *MainMenu1;
	TMenuItem *Plik1;
	TMenuItem *Otwrz1;
	TOpenDialog *OpenDialog;
	TPageControl *PageControl1;
	TMenuItem *Zapisz1;
	TMenuItem *Zapiszjako1;
	TPanel *FileStats;
	TLabel *FileInfo;
	TPanel *WelcomeInfo;
	void __fastcall Otwrz1Click(TObject *Sender);
	void __fastcall Zapisz1Click(TObject *Sender);
	void __fastcall Zapiszjako1Click(TObject *Sender);
	void __fastcall ChangeActiveTab(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall changeTab();
	AnsiString __fastcall calculateFileStats();

private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
	AnsiString appName = "Notatnik --";
    AnsiString statsSeparator = " | ";
	AnsiString titleSeparator = " | ";
	FilesFormat files[7];
	int nextEmptySlot = 0;
	int currentOpen = 0;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
