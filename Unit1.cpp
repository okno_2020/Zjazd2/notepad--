//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Otwrz1Click(TObject *Sender)
{
	if(OpenDialog->Execute()) {
		AnsiString newFile = OpenDialog->FileName;
		if ( FileExists(newFile) ) {
			files[nextEmptySlot].fileName = ExtractFileName(newFile);
			files[nextEmptySlot].filePath = newFile;
			files[nextEmptySlot].textFile = true;
			currentOpen = nextEmptySlot;
			nextEmptySlot ++;

			TTabSheet *NewTab;
			NewTab = new TTabSheet(PageControl1);
			NewTab->PageControl = PageControl1;
			NewTab->Caption = files[currentOpen].fileName;
			files[nextEmptySlot].TabSheet = NewTab;

			TRichEdit *RichEdit;
			RichEdit = new TRichEdit(NewTab);
			RichEdit->Parent = NewTab;
			RichEdit->Align = alClient;
			RichEdit->ScrollBars = ssBoth;
			RichEdit->Lines->LoadFromFile(files[currentOpen].filePath);
			files[nextEmptySlot].RichEdit = RichEdit;

			PageControl1->TabIndex = currentOpen;
            WelcomeInfo->Visible = false;

			this->changeTab();

		} else {
			Application->MessageBox(L"Plik nie istnieje", L"B��dny plik", MB_OK | MB_ICONERROR);
		}

	}
}
//---------------------------------------------------------------------------

TStringList *Tokenize (String s,String delimiter) {
	TStringList *StringList;
	int l; //length of s
	int start,delimp;
	String sub;
	StringList = new TStringList();
	start=0;
	if (s.Pos(delimiter)>0) {
		delimp = s.Pos(delimiter);
		do  {
			l=s.Length();
			sub=s.SubString(start,delimp-1);
			if (sub != "") StringList->Add(sub);
			s=s.SubString(delimp+1,l-delimp);
			delimp = s.Pos(delimiter);
		} while (delimp != 0);
		StringList->Add(s); // dodanie tego co zosta�o na ko�cu
	}
	else StringList->Add(s);
	return StringList;
}

AnsiString __fastcall TForm1::calculateFileStats(){
   Tokenize(files[currentOpen].RichEdit->Text)
   return "352 s��w, 45 wyraz�w, 4 zda�, 1 akapit";
}

void __fastcall TForm1::changeTab(){
	Form1->Caption = appName + titleSeparator  + files[currentOpen].fileName;
	FileInfo->Caption = calculateFileStats() + statsSeparator + files[currentOpen].filePath;
}

void __fastcall TForm1::Zapisz1Click(TObject *Sender)
{
	Application->MessageBox(L"Zapisz", L"Nie zaimplementowane", MB_OK | MB_ICONERROR);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Zapiszjako1Click(TObject *Sender)
{
	Application->MessageBox(L"Zapisz jako", L"Nie zaimplementowane", MB_OK | MB_ICONERROR);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ChangeActiveTab(TObject *Sender)
{
	currentOpen = PageControl1->TabIndex;
    this->changeTab();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Form1->Caption = appName;
    FileInfo->Caption = "Brak pliku";
}
//---------------------------------------------------------------------------


